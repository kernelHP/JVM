package org.hepeng.jvm.methodarea;

import java.util.concurrent.TimeUnit;

/**
 * 测试 静态变量在虚拟机中的存储位置
 *
 *
 * -Xms200M -Xmx200M -XX:MaxMetaspaceSize=300M -XX:MetaspaceSize=300M -XX:+PrintGCDetails
 *
 * @author he peng
 */
public class WhereStaticVarInJVMTest {

    static byte[] bytes = new byte [1024 * 1024 * 100];

    public static void main(String[] args) {
        System.out.println("WhereStaticVarInJVMTest started ... ");
        try {
            TimeUnit.MINUTES.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
