package org.hepeng.jvm.methodarea;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/**
 *
 * JVM Method Area 内存溢出
 *
 * -Xms60M -Xmx60M -XX:MaxMetaspaceSize=30M -XX:MetaspaceSize=30M
 *
 *
 * @author he peng
 */
public class JVMMethodAreaOOM {

    static class Obj {

    }

    public static void main(String[] args) {

        int count = 0;

        try {
            while (true) {
                Enhancer enhancer = new Enhancer();
                enhancer.setSuperclass(Obj.class);
                enhancer.setUseCache(false);
                enhancer.setCallback((MethodInterceptor) (obj, method, args1, proxy) -> proxy.invokeSuper(obj , args1));
                enhancer.create();
                count++;
            }
        } finally {
            System.out.println("count = " + count);

        }
    }

}
