package org.hepeng.jvm.methodarea;

import java.util.concurrent.TimeUnit;

/**
 * -Xms60M -Xmx60M -XX:MaxMetaspaceSize=30M -XX:MetaspaceSize=20M
 *
 * MetaspaceSize 指定是初始高水位线，一旦 Metaspace 占用内存达到这个值就会触发 Full GC。然后这个高水位线会被重置。
 * 新的高水位线值取决于GC后释放了多少 Metaspace 内存。如果释放的空间不足，那么在不超过 MaxMetaspaceSize 时，适当
 * 提高该值。如果释放空间过多，则适当降低该值。
 *
 * 为了防止频繁的 Full GC 建议把 -XX:MetaspaceSize 设置一个较高的值
 *
 * @author he peng
 */
public class MethodAreaTest {

    public static void fun1() {
        int i = 0;
        i = i++;
        System.out.println("fun1 i = " + i);
    }

    public static void fun2() {
        int i = 0;
        i = ++i;
        System.out.println("fun2 i = " + i);

    }

    public static void main(String[] args) {

        fun1();
        fun2();
        System.out.println("start...");
        try {
            TimeUnit.MINUTES.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("end...");

    }
}
