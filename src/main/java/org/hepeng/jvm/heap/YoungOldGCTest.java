package org.hepeng.jvm.heap;

import java.util.concurrent.TimeUnit;

/**
 * 测试大对象直接进入老年代
 *
 * -Xms60M -Xmx60M -XX:NewRatio=2 -XX:SurvivorRatio=8 -XX:+PrintGCDetails
 *
 * @author he peng
 */
public class YoungOldGCTest {

    public static void main(String[] args) {
        byte[] bytes = new byte[1024 * 1024 * 20];
        try {
            TimeUnit.SECONDS.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
