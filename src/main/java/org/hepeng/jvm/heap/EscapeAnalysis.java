package org.hepeng.jvm.heap;

/**
 * 对象逃逸分析
 *
 * -Xms60M -Xmx60M -XX:+PrintEscapeAnalysis
 * VM option 'PrintEscapeAnalysis' is notproduct and is available only in debug version of VM.
 *
 * 快速判断是否发生了逃逸：
 *  new 的对象是否有可能在函数外暴露。
 *
 *
 * @author he peng
 */
public class EscapeAnalysis {

    public EscapeAnalysis instance;

    /**
     * 函数返回了 instance 对象， 发生了对象逃逸
     * @return
     */
    public EscapeAnalysis getInstance() {
        return instance == null ? new EscapeAnalysis() : instance;
    }

    /**
     * 为成员属性赋值发生了逃逸
     */
    public void setInstance() {
        this.instance = new EscapeAnalysis();
    }

    /**
     * 对象的暴漏范围仅在当前函数内没有发生逃逸
     */
    public void useEscapeAnalysis() {
        EscapeAnalysis ea = new EscapeAnalysis();
    }


    /**
     * 引用成员变量的值发生了逃逸
     */
    public void useEscapeAnalysis1() {
        EscapeAnalysis ea = getInstance();
    }

    public static void main(String[] args) {
        new EscapeAnalysis().useEscapeAnalysis();
    }
}
