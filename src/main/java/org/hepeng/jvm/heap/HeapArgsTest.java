package org.hepeng.jvm.heap;

import java.util.concurrent.TimeUnit;

/**
 * 测试堆空间常用参数：
 *
 * -XX:+PrintFlagsInitial : 查看所有参数的默认初始值
 * -XX:+PrintFlagsFinal : 查看所有参数的最终值
 * -Xms : 初始堆空间大小，默认为物理内存的 1/64
 * -Xmx : 最大堆空间内存 ， 默认为物理内存的 1/4
 * -Xmn : 新生代内存大小
 * -XX:NewRatio : 配置新生代和老年代堆内存的占比,比如值为2，则Old Generation是 Yong Generation的2倍，即Yong Generation占据内存的1/3
 * -XX:NewSize : 设置Yong Generation的初始值大小
 * -XX:MaxnewSize：设置Yong Generation的最大值大小
 * -XX:SurvivorRatio : 设置新生代中 Eden 和 S0 , S1 空间的比列,比如值为5，即Eden是To(S2)的比例是5，（From和To是一样大的），此时Eden占据Yong Generation的5/7
 * -XX:MaxTenuringThreshold : 设置新生代垃圾的最大年龄
 * -XX:PrintGCDetails : 输出详细的GC处理日志
 * -XX:+PrintGC , -verbose:gc 打印简要的GC信息
 * -XX:HandlePromotionFailure : 是否设置空间分配担保
 *
 *
 *
 * @author he peng
 */
public class HeapArgsTest {

    public static void main(String[] args) {

        try {
            TimeUnit.MINUTES.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
