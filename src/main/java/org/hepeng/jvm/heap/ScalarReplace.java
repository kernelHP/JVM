package org.hepeng.jvm.heap;

import java.util.concurrent.TimeUnit;

/**
 * 标量替换测试
 *
 * 标量是不可分割的最小单元 ， Java 对象是聚合量 ， Java 中的基本数据类型就是标量。
 *
 * -server -Xms60M -Xmx60M -XX:-EliminateAllocations -XX:-DoEscapeAnalysis -XX:+PrintGCDetails
 *
 * -server -Xms60M -Xmx60M -XX:+EliminateAllocations -XX:+DoEscapeAnalysis -XX:+PrintGCDetails
 *
 * 未开启标量替换耗时 51 ms ， 开启了标量替换耗时为 4 ms ，
 * 开启了标量替换可以将标量变量直接分配在栈上，不会再去堆内存分配空间。也就不会再发生 GC 。
 *
 * @author he peng
 */
public class ScalarReplace {

    static class User {
        int id;
        String name;
    }

    private static void alloc() {
        User u = new User();
        u.id = 100;
        u.name = "小明";
    }

    public static void main(String[] args) {

        long startTime = System.currentTimeMillis();

        for (int i = 0 ; i < 100_000_00 ; i++) {
            alloc();
        }

        long end = System.currentTimeMillis();

        System.out.println("花费时间是： " + (end - startTime) + " ms");

        try {
            TimeUnit.MINUTES.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
