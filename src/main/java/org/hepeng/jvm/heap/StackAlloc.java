package org.hepeng.jvm.heap;

import java.util.concurrent.TimeUnit;

/**
 * 对象栈上分配测试
 *
 * -server -Xms1G -Xmx1G -XX:-DoEscapeAnalysis -XX:+PrintGCDetails
 * -server -Xms1G -Xmx1G -XX:+DoEscapeAnalysis -XX:+PrintGCDetails
 *
 * 允许逃逸分析可以提高性能，增加未能逃逸的对象数量可以减少 GC 的发生。
 *
 * -server -Xms256M -Xmx256M -XX:-DoEscapeAnalysis -XX:+PrintGCDetails
 * -server -Xms256M -Xmx256M -XX:+DoEscapeAnalysis -XX:+PrintGCDetails
 *
 *
 *
 * @author he peng
 */
public class StackAlloc {

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();

        for (int i = 0 ; i < 100_000_00 ; i++) {
            alloc();
        }

        long end = System.currentTimeMillis();

        System.out.println("花费时间是： " + (end - startTime) + " ms");

        try {
            TimeUnit.MINUTES.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private static void alloc() {
        Obj obj = new Obj();
    }

    static class Obj {

    }
}
